def getcube(n):
    cube_root = round(n**(1./3.))
    if round(cube_root) ** 3 == n:
        return cube_root
    return -1

x = int(input())
y = getcube(x)
print(int(y))

