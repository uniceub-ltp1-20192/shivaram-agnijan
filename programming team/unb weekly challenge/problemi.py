x = input()
x = x.split()
x = list(map(int, x))
initial = input()
initial = initial.split()
initial = list(map(int, initial))
k = int(input())
c = initial
z = 0

for i in range(0, k):
    y = input()
    y = y.split()
    y = list(map(int, y))
    if 0 < c[0] + k*y[0] <= x[0] and 0 < c[1] + k*y[1] <= x[1]:
        z += k
        c[0] += k * y[0]
        c[1] += k * y[1]
        continue
    for b in range(0, k):
        c[0] += y[0]
        c[1] += y[1]
        if c[0] <= 0 or c[1] <= 0 or c[0] > x[0] or c[1] > x[1]:
            c[0] -= y[0]
            c[1] -= y[1]
            break
        z += 1

print(z)