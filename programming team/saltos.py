
def askString():
    x = input()
    a, b, c, d, e, f, g = x.split(" ")
    a, b, c, d, e, f, g = float(a), float(b), float(c), float(d), float(e), float(f), float(g)
    return a, b, c, d, e, f, g

def askD():
    x = float(input())
    if x <= 1.2 and x >= 3.8:
        print("Valor inválido.")
    return x

def media(a, b, c, d, e, f, g):
    x = [a, b, c, d, e, f, g]
    x.sort()
    del x[0]
    del x[-1]
    y = sum(x)/5
    return y


z = askD()
a, b, c, d, e, f, g = askString()
m = media(a, b, c, d, e, f, g)
print("%.1f" % (m*z))

