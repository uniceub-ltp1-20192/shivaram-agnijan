def askString():
    x = input()
    a, b = x.split(" ")
    a, b = a.upper(), b.upper()
    y = ["PEDRA", "PAPEL", "TESOURA", "LAGARTO", "SPOCK"]
    if not(a and b) in y:
        print()
    return a, b

def resultmsg(sheldon, raj):
    if sheldon == raj:
        print("De novo!")
        return

    if sheldon == "TESOURA" and (raj == "PAPEL" or raj == "LAGARTO"):
        print("Bazinga!")
        return

    if sheldon == "PAPEL" and (raj == "PEDRA" or raj == "SPOCK"):
        print("Bazinga!")
        return

    if sheldon == "PEDRA" and (raj == "LAGARTO" or raj == "TESOURA"):
        print("Bazinga!")
        return

    if sheldon == "LAGARTO" and (raj == "SPOCK" or raj == "PAPEL"):
        print("Bazinga!")
        return
    if sheldon == "SPOCK" and (raj == "TESOURA" or raj == "PEDRA"):
        print("Bazinga!")
        return

    print("Raj trapaceou!")
    return




sheldon, raj = askString()
resultmsg(sheldon, raj)



