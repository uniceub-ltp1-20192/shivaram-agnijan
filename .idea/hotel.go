package main

import (
	"fmt"
	"strings"
)


func main(){
	name := askString("Nome:")
	vn := checkString(name, "1234567890!@#$%¨&*()<>.,;:?/")
	sobrenome := askString("Sobrenome:")
	vs := checkString(sobrenome, "1234567890!@#$%¨&*()<>.,;:?/")
	gender := askString("Genero (F/M):")
	time := askString("Hora (Ex.: 23:11):")

	if !(gender == "F" || gender == "M") {
		fmt.Println("Entre um genero válido (F/M).")
		return
	}

	if !(vn&&vs) {
		fmt.Println("Não sabe nem o que é um nome")
		return
	}

	vv := validateVogals(name)

	if !vv {
		fmt.Println("Não é suspeito")
		return
	}

	weapon := getWeapon(sobrenome)
	vw := validateAssassin(!(getGender(gender)), weapon)
	vm := validateAssassin(getGender(gender), checkTime(time))

	if !(vm||vw) {
		fmt.Println("Não é suspeito")
		return
	}
	fmt.Println("Assassino(a)!!!!!!!!!111")
}

func askString(msg string) string{
	var x string
	fmt.Println(msg)
	fmt.Scanln(&x)
	x = strings.ToUpper(x)
	return x
}

func checkString(s string, chars string) bool{
	return !strings.ContainsAny(s, chars)
}

func validateVogals(name string) bool{
	count := strings.Count(name, "A")
	count += strings.Count(name, "E")
	count += strings.Count(name, "I")

	if count >= 3 {
		return true
	}
	return false
}

func getGender(x string) bool {
	if x == "F" {
		return false
	}
	return true
}

func getWeapon(x string) bool {
	y := []string{"FACA", "CORDA", "CÁLICE"}
	for _, b := range y{
		if b == x {
			return true
		}
	}
	return false
}

func validateAssassin(x bool, y bool) bool {
	if x && y {
		return true
	}
	return false
}



func checkTime(y string) bool{
	if y == "00:30" {
		return true
	}
	return false
}