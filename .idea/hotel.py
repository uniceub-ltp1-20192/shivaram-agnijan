#No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
#A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
#Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.
import sys

def askString(msg):
	x = input(msg)
	return x.upper()

def validateString(s):
	return s.replace(" ","").isalpha() #checks if its only alphabets, eliminating the space

def validateVogals(name): #checks first rule
	count = name.count("A")
	count += name.count("E")
	count += name.count("I")
	if count >= 3:
		return True
	return False

def getBioGender(x):
	if x == "F":
		return False
	return True

def getWeapon(x):
	whiteweapons = ["FACA", "CÁLICE", "CORDA"]
	if x in whiteweapons:
		return True
	return False

def validateAssassin(x, y):
	if x and y:
		return True
	return False

def checkTime(y):
	if y == "00:30":
		return True
	return False


# solcitar nome ao usuário
name = askString("Nome:")
vn = validateString(name)
surname = askString("Sobrenome:")
vs = validateString(surname)
gender = askString("Genero (F/M):")
time = askString("Hora (Ex.: 23:11):")

if not(gender=="F" or gender=="M"):
	print("Entre um genero válido (F/M).")

if not (vn and vs):
	sys.exit("Não sabe nem o que é um nome")

# verifica se nome respeita a RN1 (vogais)
vv = validateVogals(name)

if not vv:
	sys.exit("Não é suspeito")

weapon = getWeapon(surname)
vw = validateAssassin(not(getBioGender(gender)), weapon)
vm = validateAssassin(getBioGender(gender), checkTime(time))

if not(vm or vw):
	sys.exit("Não é suspeito")

print("ASSSSAAASSSIIINO(AAA)!!!!!!!!!!!!!!!!!!!!!!!!111")	



