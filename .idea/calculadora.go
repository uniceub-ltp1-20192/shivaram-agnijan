package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	number_one, number_two := askNumber("Digite um número:")
	operation:= askopperator("Operação: ")
	printResult(calculate(number_one, number_two, operation))
}

func askopperator(msg string) string {
	var input string
	k := 1
	for k < 2 {
		fmt.Print(msg)
		fmt.Scanln(&input)
		if !strings.ContainsAny(input, "+-/*") {
			fmt.Println("Digite um operador válido.")
			continue
		}
		if len(input) > 1 {
			fmt.Println("Digite somente uma operação por vez.")
			continue
		} else {
			k++
		}
	}
	return input
}

func askNumber(msg string) (float64, float64) {
	var firstnum string
	var secondnum string
	k := 0
	for k < 1 {
		fmt.Print(msg)
		fmt.Scanln(&firstnum)
		fmt.Print(msg)
		fmt.Scanln(&secondnum)
		_, err := strconv.ParseFloat(firstnum, 64)
		_, erro := strconv.ParseFloat(secondnum, 64)
		if err != nil || erro != nil {
			fmt.Println(err, "\n", erro)
			fmt.Println("Somente números são permitidos.")
			continue
		} else {
			k++
		}
	}
	x, _:= strconv.ParseFloat(firstnum, 64)
	y, _:= strconv.ParseFloat(secondnum, 64)
	return x, y
}



func calculate(n float64, m float64, op string) float64{

	if (op == "+") {
		return n + m
	}
	if (op == "-") {
		return n - m
	}
	if (op == "*") {
		return n * m
	}
	return n / m
}

func printResult(number interface{}) {
	fmt.Printf("O resultado é: %v \n", number)
}




