def askproducts():
    a, b = 0, 0
    while True:
        try:
            x = int(input("Digite 1 se deseja inserir algo ou 2 se deseja finalizar:"))
            if x == 2:
                break
            if x != 1:
                raise TypeError
            y = float(input("Qual o valor?"))
            z = float(input("Qual a porcentagem d e desconto (0-100):"))
            if y < 0 or z < 0:
                raise NameError
            a += y
            b += y*((100-z)/100)
        except ValueError:
            print("Digite somente numeros.\n")
        except NameError:
            print("Digite um valor positivo.\n")
        except TypeError:
            print("Digite somente 1 ou 2.\n")
    return print("Voce pagaria:", a, "\nVoce vai pagar:", b)






