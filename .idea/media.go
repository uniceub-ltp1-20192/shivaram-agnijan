package main

import (
	"fmt"
	"strings"
	"strconv"
)

func main() {
	AskNumber("Digite as notas separadas por um asterisco (*) com no máximo 1 casa decimal: ")
}

func checkfloat(x string) float64 {
    y := strings.Split(x, "*")
    total := float64(0)
    for k := 0; k < 3; k++ {
        a, erro := strconv.ParseFloat(y[k], 64)
        if erro != nil {
            fmt.Println(erro, "\nDigite os números corretamente.")
			continue
        }
        total += a
    }
    return total/3
}


func AskNumber(x string) {
	var y string
	fmt.Print(x)
	fmt.Scanln(&y)
	checklength(y)
	k := checkfloat(y)
	fmt.Println("A média é", k)
}

func checklength(x string) {
	if len(x) < 5 {
		fmt.Println("Digite todos os 3 números separados por asterisco.\n")
		AskNumber("Digite as notas separadas por um asterisco (*) com no máximo 1 casa decimal: ")
	}
}
