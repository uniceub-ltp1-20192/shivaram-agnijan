def partida():
    x, primeirajogada = 0, True
    n = int(input("Informe o número de peças inicial:"))
    m = int(input("Informe o número máximo de pças que podem ser retiradas em 1 rodada:"))

    while True:

        if n != 0 and (m + 1) % n != 0 and primeirajogada:
            n, x = computador_escolhe_jogada(n, m)
            print("Número restante:", n, "\nPeças removidas pelo computador:", x, "\n")

        n, x = usuario_escolhe_jogada(n, m)
        primeirajogada = False

        if n == 0:
                print("Você ganhou!")
                break

        print("Número restante:", n, "\nPeças removidas na última jogada:", x, "\n")
        n, x = computador_escolhe_jogada(n, m)

        if n == 0:
                print("O computador ganhou!")
                break

        print("Número restante:", n, "\nPeças removidas na última jogada pelo computador:", x, "\n")


def computador_escolhe_jogada(n, m):
    x = 0
    while True:
        if (n - x) % (m + 1) == 0:
            return (n - x), x

        x += 1


def usuario_escolhe_jogada(n, m):
    while True:

        try:
            x = int(input("Informe quantas peças irá retirar:"))

            if x > m:
                raise NameError
            return (n - x), x

        except TypeError:
            print("Escreva somente inteiros.\n")
        except NameError:
            print("Número maior que numero maximo de peças permitido retirar.\n")
    

partida()