from random import shuffle

SUITS          = ("SPADES", "HEARTS", "DIAMONDS", "CLUBS")
Selecteddeck = [1, 2, 3, 4, 5, 6, 7, 11, 12, 13]

def generatetrucoDeck():
    cards = []
    for suit in SUITS:
        for value in Selecteddeck:
            card = (suit, value)
            cards.append(card)
    return cards

def generateShuffleDeck():
        deck = generatetrucoDeck()
        shuffle(deck)
        return deck

def dealer(x):
    handone, handtwo, handthree, handfour = x[0:3], x[3:6], x[6:9], x[9:12]
    return handone, handtwo, handthree, handfour

def jogada(x, deck):
    y = int(input("Escolha uma carta e digite seu respectivo número (1,2,3):")) - 1
    deck.append(x[y])
    return print(deck)

def compare(deck):
    x, y, z, k = (deck[0][1]), (deck[1][0]), (deck[0][0]), (deck[1][1])

    if x <= 3 and 3 < k <= 13 or 3 >= x > k <= 3 or x >= 3 and 3 <= k < x:
        return deck[0], True
    if x == k and z == "CLUBS" or x == k and z == "HEARTS" and y != "CLUBS" or x == k and z == "SPADES" and y == "DIAMONS":
        return deck[0]
    return deck[1], False

def rodada(jogador, deck):
    print("Mão:\n", jogador)
    jogada(jogador, deck)

def turno():
    timeum, timedois = 0, 0
    while timeum < 12 and timedois < 12:
        monte, final = [], []
        jogador1, jogador2, jogador3, jogador4 = dealer(generateShuffleDeck())
        rodada(jogador1, monte)
        rodada(jogador2, monte)
        deck, turn = compare(monte)
        final.append(deck)
        monte = []
        rodada(jogador3, monte)
        rodada(jogador4, monte)
        decktwo, turntwo = compare(monte)
        final.append(decktwo)
        deckfinal, turnfinal = compare(final)
        if turn and turntwo or turn and turnfinal:
            timeum += 1
            print("A carta que ganhou é:", deckfinal, "do Time 1."), timeum, timedois
            print("Pontos time1:", timeum, "\nPontos time2:", timedois)
        else:
            timedois += 1
            print("A carta que ganhou é:", deckfinal, "do Time 2"), timeum, timedois
            print("Pontos time1:", timeum, "\nPontos time2:", timedois)
    if timeum < 12:
        return print("Time 2 ganhou.")
    return print("Time 1 ganhou.")

turno()

