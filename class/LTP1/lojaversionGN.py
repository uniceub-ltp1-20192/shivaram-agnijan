def getvalue(msg, percentage):
    while True:
        try:
            y = float(input(msg))
            if y < 0:
                raise NameError
            if percentage and y > 100:
                raise TypeError
            break
        except NameError:
            print("Digite um valor positivo.\n")
        except ValueError:
            print("Digite somente numeros.\n")
        except TypeError:
            print("Digite de 0-100.\n")
    return y

def menu():
    while True:
        try:
            x = int(input("Digite 1 se deseja inserir algo ou 2 se deseja finalizar:"))
            if x == 2:
                return False
            if x != 1:
                raise TypeError
            return True
        except ValueError:
            print("Digite somente numeros.\n")
        except TypeError:
            print("Digite somente 1 ou 2.\n")

def askproducts():
    a, b = 0, 0
    while menu():
            x = getvalue("Qual o valor?", False)
            a += x
            b += x*(100-getvalue("Qual a porcentagem de desconto (0-100):", True))/100
    return print("Voce pagaria:", a, "\nVoce vai pagar:", b)

askproducts()



