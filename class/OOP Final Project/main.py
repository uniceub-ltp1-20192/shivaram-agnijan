from Menu.menu import Menu
from Dados.dados import Dados
import os


if os.path.exists("dados.txt"):
    dados = Dados(Dados.readFile("dados.txt"))
else:
    dados = Dados()



dados = Menu.Menuprincipal(dados)
dados.saveFile("dados.txt")


"""Feito por Shivaram A. P., Breno Amaral, Diego Figueiro"""