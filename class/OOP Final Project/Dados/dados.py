from Validador.validador import Validador
from Entidades.human import Human
from Entidades.indian import Indian

class Dados:
    def __init__(self, dicionario=dict()):
        self._dicionario = dicionario

    @property
    def dicionario(self):
        return self._dicionario


    def add(self, indian = False):
        hashnumber   = self.Gerador()
        name      = Validador.getName()
        age   = Validador.getAge()
        height = Validador.getHeight()
        
        if indian:
            gender     = Validador.getGender()
            MathSkillz = Validador.getMathSkillz()
            self._dicionario[hashnumber] = Indian("In"+hashnumber, name, age, height, gender, MathSkillz)
        else:
            self._dicionario[hashnumber] = Human("Hu"+hashnumber, name, age, height)
        self._dicionario[hashnumber].printValues()  


    def change(self, item):
        if item in self._dicionario:
            print("\nValores antigos:\n")
            self._dicionario[item].printValues()
            print("Valores novos:\n")
            name      = Validador.getName()
            age   = Validador.getAge()
            height = Validador.getHeight()

            if isinstance(self._dicionario[item], Indian):
                gender     = Validador.getGender()
                MathSkillz = Validador.getMathSkillz()
                self._dicionario[item] = Indian("In"+item, name, age, height, gender, MathSkillz)
            else:
                self._dicionario[item] = Human("Hu"+item, name, age, height)
            self._dicionario[item].printValues()
            print("\nItem alterado.")

        else:
            print("O item não existe.")
    

    def excluir(self, item):
        if item in self._dicionario:
            self._dicionario.pop(item)
            print("\nItem excluído.")
        else:
            print("\nO item não existe.")


    def consultHash(self, hash):
        if hash in self._dicionario:
            self._dicionario[hash].printValues()
        else:
            print("Busca finalizada, nada encontrado....\n")


    def consultaratributo(self, atributo):
        Null = True
        for x in self._dicionario:
            if self._dicionario[x].hasattribute(atributo):
                self._dicionario[x].printValues()
                Null = False
        if Null:
            print("Busca finalizada, nada encontrado....\n")


    def Gerador(self):
        chave = 0
        while str(chave) in self._dicionario:
            chave += 2
        return str(chave)


    def Vazio(self):
        if len(self._dicionario) == 0:
            return True
        return False


    @staticmethod
    def readFile(arquivo):
        file = open(arquivo, "r")
        dados = dict()
        for k in file:
            if k[0] == "Hu":
                x = k.split("/")
                dados[c[0][1:]] = Human(c[0], c[1], int(c[2]), c[3])
            elif k[0] == "In":
                x = k.split("/")
                dados[c[0][1:]] = Indian(c[0], c[1], int(c[2]), c[3], c[4], c[5])
        file.close()
        return dados

    def saveFile(self, arquivo):
        file = open(arquivo, "w")
        for k in self._dicionario:
            file.write( self._dicionario[k].txtformat() + "\n" )
        file.close