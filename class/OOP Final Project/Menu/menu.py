from Validador.validador import Validador
from Dados.dados import Dados
from os import system
import re

class Menu:

    @classmethod
    def Menuprincipal(cls, dados):
        while True:
            system('cls')
            option = cls.menurun()
            if option == "0":
                print("\nSaindo...\n")
                return dados
            elif option == "1":
                cls.consultarmenu(dados)
            elif option == "2":
                dados = cls.inserirmenu(dados)
            elif option == "3":
                dados = cls.alterarmenu(dados)
            elif option == "4":
                dados = cls.excluirmenu(dados)


    @staticmethod
    def menurun():
        print("==Menu==\n0 – Sair\n1 – Consultar\n2 – Inserir\n3 – Alterar\n4 – Excluir\n")
        return Validador.validarmenurun("[0-4]")
    
    
    @classmethod
    def consultarmenu(cls, dados, detalhe=""):
        system('cls')
        if dados.Vazio():
            print("Dados vazios.\n")
            return input("continuar...\n")

        while True:
            option = cls.consultarmenurun(detalhe)
            if option == "1":
                identificador = Validador.getHash("\nDigite o identificador: ")
                dados.consultHash(identificador) 
            elif option == "2":
                attribute = input("\nDigite o atributo: ")
                dados.consultaratributo(attribute.lower())  
            if option == "0" or detalhe != "":
                break
            system('cls')


    @staticmethod
    def consultarmenurun(detalhe):
        print("Menu {}Consulta:\n\n0 – Voltar\n1 – Consultar por Identificador\n2 – Consultar por Atributo\n".format(detalhe))
        return Validador.validarmenurun("[0-2]")
    
    
    @classmethod
    def inserirmenu(cls, dados):
        system('cls')
        while True:
            system('cls')
            option = cls.inserirmenurun()
            system('cls')
            if option == "0":
                return dados
            elif option == "1":
                print("Menu, Inserir => Humano\n")
                dados.add()
            elif option == "2":
                print("Menu, Inserir => Indiano\n\n")
                dados.add(indian=True)
            print("\nDados armazenados.")


    @staticmethod
    def inserirmenurun():
        print("Menu, Inserir\n\n0 – Voltar\n1 – Inserir humano\n2 – Inserir indiano\n")
        return Validador.validarmenurun("[0-2]")
    
    
    @classmethod
    def alterarmenu(cls, dados):
        system('cls')
        if dados.Vazio():
            print("Dados vazios.\n")
            return dados
        option = "2"
        while True:
            if option == "2":
                cls.consultarmenu(dados, "Alteração:")
            option = cls.alterarmenurun()
            if option == "0":
                return dados
            elif option == "1":
                identificador = Validador.getHash("\nDigite o hash do item a ser alterado: ")
                system('cls')
                dados.change(hash)
            system('cls')


    @staticmethod
    def alterarmenurun():
        print("Menu, Alteração:\n\n0 – Voltar\n1 – Alterar\n2 – Consultar\n")
        return Validador.validarmenurun("[0-2]")


    @classmethod
    def excluirmenu(cls, dados):
        system('cls')
        if dados.Vazio():
            print("Dados vazios.\n")
            return dados
        option = "2"
        while True:
            if option == "2":
                cls.consultarmenu(dados, "Exclusão: ")
            option = cls.excluirmenurun()
            if option == "0":
                return dados
            elif option == "1":
                hash = Validador.getHash("\nDigite o identificador do item a ser excluído: ")
                dados.excluir(hash)
            system('cls')


    @staticmethod
    def excluirmenurun():
        print("Menu, Exclusão:\n\n0 – Voltar\n1 – Excluir\n2 – Consultar\n")
        return Validador.validarmenurun("[0-2]")