import re

class Validador:
    
    @staticmethod
    def getHash(texto):
        while True:
            x = input(texto)
            palavra = re.match("\d+", x)
            if palavra:
                return x
            else:
                print("Infome um valor inteiro.")
    
    @staticmethod
    def validarmenurun(expReg):
        while True:
            y = input("Digite a opção:")
            k  = re.match(expReg, y)
            if k != None:
                return y
            else:
                print("Escolha inválida! Infome um valor dentre: {}.".format(expReg))

    
    @staticmethod
    def getName():
        while True:
            name = input("Nome: ")
            
            if name == "":
                print("\nPor favor digite um nome válido.")
            else:
                return name.lower()

    @staticmethod
    def getAge():
        while True:
            age = input("Idade em anos: ")
            age = re.sub("\D", "", age)
            
            if Validador.checkAge(age):
                return int(age)
            else:
                print("\nInsira uma idade válida.")

    @staticmethod
    def getHeight():
        while True:
            height = input("Altura em cm: ")
            height = height.lower()
            height = re.sub("\D", "", height)

            if Validador.checkHeight(height):
                return "".join(height)
            else:
                print("\nInsira uma altura válida, caso tenha inserido +800, gigantes não existem.")

    @staticmethod
    def getGender():
        while True:
            gender = input("Nome do pai: ")
            if gender == "":
                print("\nInsira um nome válido.")
            else:
                return gender.lower()

    @staticmethod
    def getMathSkillz():
        while True:
            ms = input("Math Skillz (0 - 10): ")
            
            if ms == "":
                print("\nInsira um número válido.")
            else:
                return ms.lower()
    
    
    @staticmethod
    def checkAge(age):
        if (age == ""
            or int(age) <= 0):
            return False

        return True

    @staticmethod
    def checkHeight(height):
        if (height == ""
            or int(height) >= 800):
            return False

        return True
