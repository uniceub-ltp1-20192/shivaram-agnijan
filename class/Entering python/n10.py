def n10o1():
    for i in range(1, 21):
        print(i)

def n10o2():
    list = []
    for i in range(1, 1000001):
        list.append(i)
    print(list)

def n10o3():
    list = []
    for i in range(1, 1000001):
        list.append(i)
    print(min(list))
    print(max(list))
    print(sum(list))

def n10o4():
    list = []
    for i in range(1, 20, 2):
        list.append(i)
        print(i)

def n10o5():
    list = []
    for i in range(3, 1000, 3):
        list.append(i)
        print(i)

def n10o6():
    list = []
    for i in range(1, 5):
        list.append(i)
    print(list)

def n10o7():
    list = [x**3 for x in range(1, 5)]
    print(list)
    print("Os primeiros 3 itens da lista são:", list[0:3])
    print("Os ultimos 3 itens da lista são:", list[2:4])
    print("Os 3 itens no meio da lista são:", list[1:3])

n10o1()
n10o2()
n10o3()
n10o4()
n10o5()
n10o6()
n10o7()