def n4o1():
    x = "  Shivaram\nAgnijan\tPalackapillil    "
    print(x)
    y = x.rstrip()
    print(y)
    y = x.lstrip()
    print(y)
    y = x.strip()
    print(y)

def n4o2():
    x = input("Entre uma frase para ser contada:")
    y = x.replace(" ", "")
    print("A frase possui", len(y), "caracteres.")

n4o1()
n4o2()