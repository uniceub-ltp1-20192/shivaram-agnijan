def n2o2():
    cebolas = 300
    cebolas_na_caixa = 120
    espaco_caixa = 5
    caixas = 60
    cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
    caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
    caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
    print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
    print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
    print("Em cada caixa cabem", espaco_caixa, "cebolas")
    print("Ainda temos,", caixas_vazias, "caixas vazias")
    print("Então, precisamos de,", caixas_necessarias, "caixas para empacotar todas as cebolas")
def n2o1():
    quantidade_cebolas = int(input("Digite a quantidade de cebolas:"))
    preco_cebolas = int(input("Digite o preço das cebolas:"))
    return print("Voce ira pagar", (quantidade_cebolas*preco_cebolas), "pelas cebolas.")

n2o1()
n2o2()