from graphics import *
import random

# paleta de cores
cor1 = color_rgb(204, 61, 51)  # laranja
cor2 = color_rgb(35, 35, 42)  # cinza escuro
cor3 = color_rgb(242, 194, 36)  # amarelo
cor4 = color_rgb(11, 184, 123)  # verde
cor5 = color_rgb(24, 161, 196)  # azul

fonte = "times roman"


def menutxt():
    menu1 = Rectangle(Point(0, 0), Point(799, 300))
    menu1.setFill(cor4)
    menu1.setOutline(cor4)
    menu1.draw(win)
    menu2 = Rectangle(Point(0, 301), Point(799, 599))
    menu2.setFill(cor4)
    menu2.setOutline(cor4)
    menu2.draw(win)

    caixa_menu = Rectangle(Point(250, 250), Point(550, 350))
    caixa_menu.setFill("white")
    caixa_menu.setOutline("white")
    caixa_menu.draw(win)

    menu = Text(Point(400, 300), "MENU")
    menu.setSize(36)
    menu.setTextColor(cor2)
    menu.setFace(fonte)
    menu.setStyle("bold")
    menu.draw(win)

    presskey = Text(Point(400, 450), "Pressione uma tecla para começar...")
    presskey.setSize(18)
    presskey.setTextColor(cor2)
    presskey.setFace("courier")
    presskey.setStyle("bold")
    presskey.draw(win)

    retangulo = Rectangle(Point(0, 0), Point(799, 140))
    retangulo.setFill(cor1)
    retangulo.setOutline(cor1)
    retangulo.draw(win)

    instruction()
    caixa_menu.undraw()

    for ind in range(0, 31):
        menu1.move(0, -10)
        menu2.move(0, 10)
        retangulo.move(0, -10)
        time.sleep(1 / 90)

    menu1.undraw()
    menu2.undraw()
    retangulo.undraw()
    menu.undraw()
    presskey.undraw()


def timer():
    timer3 = Text(Point(400, 300), "3.")
    timer3.setSize(35)
    timer3.setTextColor("white")
    timer3.setFace(fonte)
    timer3.setStyle("bold")
    timer3.draw(win)
    time.sleep(1 / 3)
    timer3.undraw()
    timer31 = Text(Point(400, 300), "3..")
    timer31.setSize(35)
    timer31.setTextColor("white")
    timer31.setFace(fonte)
    timer31.setStyle("bold")
    timer31.draw(win)
    time.sleep(1 / 3)
    timer31.undraw()
    timer32 = Text(Point(400, 300), "3...")
    timer32.setSize(35)
    timer32.setTextColor("white")
    timer32.setFace(fonte)
    timer32.setStyle("bold")
    timer32.draw(win)
    time.sleep(1 / 3)
    timer32.undraw()
    timer2 = Text(Point(400, 300), "2.")
    timer2.setSize(35)
    timer2.setTextColor("white")
    timer2.setFace(fonte)
    timer2.setStyle("bold")
    timer2.draw(win)
    time.sleep(1 / 3)
    timer2.undraw()
    timer22 = Text(Point(400, 300), "2..")
    timer22.setSize(35)
    timer22.setTextColor("white")
    timer22.setFace(fonte)
    timer22.setStyle("bold")
    timer22.draw(win)
    time.sleep(1 / 3)
    timer22.undraw()
    timer23 = Text(Point(400, 300), "2...")
    timer23.setSize(35)
    timer23.setTextColor("white")
    timer23.setFace(fonte)
    timer23.setStyle("bold")
    timer23.draw(win)
    time.sleep(1 / 3)
    timer23.undraw()
    timer1 = Text(Point(400, 300), "1.")
    timer1.setSize(35)
    timer1.setTextColor("white")
    timer1.setFace(fonte)
    timer1.setStyle("bold")
    timer1.draw(win)
    time.sleep(1 / 3)
    timer1.undraw()
    timer12 = Text(Point(400, 300), "1..")
    timer12.setSize(35)
    timer12.setTextColor("white")
    timer12.setFace(fonte)
    timer12.setStyle("bold")
    timer12.draw(win)
    time.sleep(1 / 3)
    timer12.undraw()
    timer13 = Text(Point(400, 300), "1...")
    timer13.setSize(35)
    timer13.setTextColor("white")
    timer13.setFace(fonte)
    timer13.setStyle("bold")
    timer13.draw(win)
    time.sleep(1 / 3)
    timer13.undraw()

    return


def instruction():
    esc = Text(Point(800/6, 50), "Para sair:")
    esc.setTextColor(cor2)
    esc.setFace(fonte)
    esc.setSize(16)
    esc.draw(win)

    escbox = Rectangle(Point(800/6 - 35/2, 70), Point(800/6 + 35/2, 100))
    escbox.setOutline(cor2)
    escbox.setWidth(3)
    escbox.draw(win)

    esctxt = Text(Point(800/6, 85), "Esc")
    esctxt.setTextColor(cor2)
    esctxt.setFace(fonte)
    esctxt.setSize(9)
    esctxt.draw(win)

    instkeys = Text(Point(400, 50), "Para se movimentar:")
    instkeys.setSize(16)
    instkeys.setFace(fonte)
    instkeys.setTextColor(cor2)
    instkeys.draw(win)

    left = Rectangle(Point(340, 70), Point(375, 100))
    left.setOutline(cor2)
    left.setWidth(3)
    left.draw(win)

    lline = Line(Point(345, 85), Point(370, 85))
    lline.setFill(cor2)
    lline.setWidth(3)
    lline.setArrow("first")
    lline.draw(win)

    right = Rectangle(Point(460, 70), Point(425, 100))
    right.setOutline(cor2)
    right.setWidth(3)
    right.draw(win)

    llline = Line(Point(455, 85), Point(430, 85))
    llline.setFill(cor2)
    llline.setWidth(3)
    llline.setArrow("first")
    llline.draw(win)

    pause = Text(Point(2000/3, 50), "Para pausar:")
    pause.setSize(16)
    pause.setFace(fonte)
    pause.setTextColor(cor2)
    pause.draw(win)

    pausebox = Rectangle(Point(2000/3 - 105/2, 70), Point(2000/3 + 105/2, 100))
    pausebox.setOutline(cor2)
    pausebox.setWidth(3)
    pausebox.draw(win)

    pausetxt = Text(Point(2000/3, 85), "Space bar")
    pausetxt.setTextColor(cor2)
    pausetxt.setFace(fonte)
    pausetxt.setSize(9)
    pausetxt.draw(win)

    win.getKey()

    esc.undraw()
    escbox.undraw()
    esctxt.undraw()
    instkeys.undraw()
    left.undraw()
    lline.undraw()
    right.undraw()
    llline.undraw()
    pause.undraw()
    pausebox.undraw()
    pausetxt.undraw()


def jogo():
    linhaSuperior = Line(Point(0, 40), Point(800, 40))
    linhaSuperior.setWidth(10)
    linhaSuperior.setFill(cor4)
    linhaSuperior.draw(win)

    linhaInferior = Line(Point(0, 550), Point(800, 550))
    linhaInferior.setWidth(3)
    linhaInferior.setFill(cor1)
    linhaInferior.draw(win)

    col = 390
    lin = 200
    circulo = Circle(Point(col, lin), 15)
    circulo.setFill("white")
    circulo.setOutline("white")
    circulo.draw(win)

    point = 0
    pontos = Text(Point(400, 575), "Pontos: 0")
    pontos.setTextColor("white")
    pontos.setSize(14)
    pontos.setFace(fonte)
    pontos.draw(win)

    colIni = 340
    barra = Line(Point(colIni, 530), Point(colIni+100, 530))
    barra.setFill(cor5)
    barra.setOutline(cor5)
    barra.setWidth(10)
    barra.draw(win)

    timer()

    # serve pra aumentar a velocidade que a barra anda
    b = 0

    contagem = 0
    continuar = True

    # velocidades iniciais
    velocidade = -5
    passo = 5

    while continuar:
        # quando bater na barra
        if (lin + 15) == 530 and colIni < col < (colIni + 100):
            # aumentar pontuação
            point += 1
            pontos.undraw()
            pontos = Text(Point(400, 575), "Pontos: " + str(point))
            pontos.setTextColor("white")
            pontos.setSize(14)
            pontos.setFace(fonte)
            pontos.draw(win)
            contagem += 1
            velocidade = -velocidade
            ran = random.random()
            if ran > 0.5:
                passo = -passo
            # acelera a velocidade em 5 a cada 5 pontos
            if contagem == 5:
                velocidade -= 5
                if passo > 0:
                    passo += 5
                else:
                    passo -= 5
                b += 10
                contagem = 0

        # bater encima ou nas laterais
        if col > 785:
            passo = -passo
        if col < 15:
            passo = -passo
        if lin < 60:
            velocidade = -velocidade

        # Nova posição do círculo
        circulo.undraw()
        col += passo
        lin += velocidade
        circulo = Circle(Point(col, lin), 15)
        circulo.setFill("white")
        circulo.setOutline("white")
        circulo.draw(win)

        # Movimento horizontal da barra pelas setas direita/esquerda
        tecla = win.checkKey()
        # Sair do joguinho
        if tecla == "Escape":
            creditos()
        if tecla == "Right":
            if (colIni + (20 + b)) < 719:
                colIni = colIni + (20 + b)
            barra.undraw()
            barra = Line(Point(colIni, 530), Point(colIni + 100, 530))
            barra.setFill(cor5)
            barra.setOutline(cor5)
            barra.setWidth(10)
            barra.draw(win)
        if tecla == "Left":
            if (colIni - (20 + b)) > -19:
                colIni = colIni - (20 + b)
            barra.undraw()
            barra = Line(Point(colIni, 530), Point(colIni + 100, 530))
            barra.setFill(cor5)
            barra.setOutline(cor5)
            barra.setWidth(10)
            barra.draw(win)
        # pausar
        if tecla == "space":
            tela_pause = Rectangle(Point(0, 0), Point(799, 599))
            tela_pause.setFill(cor3)
            tela_pause.draw(win)
            pause = Text(Point(400, 300), "JOGO PAUSADO")
            pause.setSize(36)
            pause.setTextColor(cor2)
            pause.setFace(fonte)
            pause.setStyle("bold")
            pause.draw(win)
            presskey = Text(Point(400, 450), "Pressione uma tecla para continuar...")
            presskey.setSize(18)
            presskey.setTextColor(cor2)
            presskey.setFace("courier")
            presskey.setStyle("bold")
            presskey.draw(win)
            instruction()
            pause.undraw()
            presskey.undraw()
            tela_pause.undraw()

        if (lin - 15) > 600:
            for kjkj in range(0, 9):
                perdeu = Text(Point(400, 300), "VOCÊ PERDEU!")
                perdeu.setSize(30)
                perdeu.setTextColor("white")
                perdeu.setFace(fonte)
                perdeu.setStyle("bold")
                perdeu.draw(win)
                time.sleep(0.2)
                perdeu.undraw()
                perdeu = Text(Point(400, 300), "VOCÊ PERDEU!")
                perdeu.setSize(36)
                perdeu.setFill(cor1)
                perdeu.setFace(fonte)
                perdeu.setStyle("bold")
                perdeu.draw(win)
                time.sleep(0.2)
                perdeu.undraw()
                tecla = win.checkKey()

                # Sair do joguinho
                if tecla == "Escape":
                    exit()
            continuar = False
            point = 0
            linhaInferior.undraw()
            linhaSuperior.undraw()
            pontos.undraw()
            barra.undraw()
            circulo.undraw()
        # Esperar o ser humano reagir
        time.sleep(1/30)

    return


def creditos():
    tela_creditos = Rectangle(Point(0, 0), Point(799, 599))
    tela_creditos.setFill(cor1)
    tela_creditos.draw(win)

    txtcreditos = Text(Point(400, 725), "Créditos:")
    txtcreditos.setSize(36)
    txtcreditos.setTextColor(cor2)
    txtcreditos.setFace(fonte)
    txtcreditos.setStyle("bold")
    txtcreditos.draw(win)

    txtcreditos1 = Text(Point(400, 800), "Breno Amaral")
    txtcreditos1.setSize(25)
    txtcreditos1.setTextColor(cor2)
    txtcreditos1.setFace(fonte)
    txtcreditos1.draw(win)

    txtcreditos2 = Text(Point(400, 850), "Diego Figueiró")
    txtcreditos2.setSize(25)
    txtcreditos2.setTextColor(cor2)
    txtcreditos2.setFace(fonte)
    txtcreditos2.draw(win)

    txtcreditos3 = Text(Point(400, 900), "Gabriel Vieira Neves")
    txtcreditos3.setSize(25)
    txtcreditos3.setTextColor(cor2)
    txtcreditos3.setFace(fonte)
    txtcreditos3.draw(win)

    txtcreditos4 = Text(Point(400, 950), "Shivaram Agnijan")
    txtcreditos4.setSize(25)
    txtcreditos4.setTextColor(cor2)
    txtcreditos4.setFace(fonte)
    txtcreditos4.draw(win)

    for d in range(0, 131):
        txtcreditos.move(0, -5)
        txtcreditos1.move(0, -5)
        txtcreditos2.move(0, -5)
        txtcreditos3.move(0, -5)
        txtcreditos4.move(0, -5)
        time.sleep(1/600)

    creditbox = Rectangle(Point(200, 40), Point(600, 100))
    creditbox.setFill("white")
    creditbox.setOutline("white")
    creditbox.draw(win)
    txtcreditos.undraw()
    txtcreditos.draw(win)
    time.sleep(1)
    txtcreditos4.undraw()
    time.sleep(0.3)
    txtcreditos3.undraw()
    time.sleep(0.3)
    txtcreditos2.undraw()
    time.sleep(0.3)
    txtcreditos1.undraw()
    time.sleep(0.3)
    exit()
    return


win = GraphWin("Esse jogo vale 3/3", 800, 600)

win.setBackground(cor2)

menutxt()

while True:
	jogo()
