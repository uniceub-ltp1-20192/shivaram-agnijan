def checkprime(x):
    if x >= 2:
        for y in range(2, x):
            if x % y == 0:
                return False
    return True

def n5o1and2():
    x = int(input("Entre o valor para checar se é primo, impar ou par:"))
    y = "Impar"
    if (x % 2) == 0:
        y = "Par"
    if not checkprime(x):
        x = "não primo."
    else:
        x = "primo."
    print("O numero é", y, "e", x)

def n5o3():
    x = int(input("Entre o primeiro numero:"))
    y = int(input("Entre o segundo numero:"))
    print("Add:", x+y)
    print("Sub:", x-y)
    print("Mult:", x*y)
    print("Div:", x/y)

def n5o4and5():
    y = int(input("Entre seu semestre:"))
    z = int(input("Quantos semestres atrasados:"))
    print("Voce vai se formar em", (8-(y-z))/2, "ano/s.")

n5o1and2()
n5o3()
n5o4and5()