def test(i, y):
    if y[i] == "<" and i > 0 and y[i-1] == ">":
        return False
    if y[i] == ">" and i == len(y) - 1:
        return True
    if y[i] == ">" and i < len(y):
        i += 1
        return test(i, y)
    if y[i] == "<" and i == 0:
        return True
    if y[i] == "<" and i > 0:
        i -= 1
        return test(i, y)

n = int(input())
y = input()
y = list(y)
x = 0
for i in range(0, n):
    if test(i, y):
        x += 1
print(x)


