def problem3():
    name = input()
    letter = "BCDEFGJKLNPQRSZ "
    for i in name:
        if i in letter:
            return print("NO")
    if name == name[::-1] and len(name) >= 1:
        return print("YES")
    return print("NO")

problem3()