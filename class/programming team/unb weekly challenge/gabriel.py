def count(a, b):
    counter = 0
    a = list(map(int, a))
    b = list(map(int, b))
    a.sort()
    b.sort()

    for i in range(len(a)-1, -1, -1):
        if len(b) == 0:
            return counter
        for x in range(len(b)-1, -1, -1):
            if 1 >= (a[i] - b[x]) >= -1:
                del a[i]
                del b[x]
                counter += 1
                break
    return counter


boys = int(input())

boys_dancing_skill = input()
boys_dancing_skill = boys_dancing_skill.split()

girls = int(input())

girls_dancing_skill = input()
girls_dancing_skill = girls_dancing_skill.split()

if boys > girls:
	print(count(boys_dancing_skill, girls_dancing_skill))
else:
	print(count(girls_dancing_skill, boys_dancing_skill))