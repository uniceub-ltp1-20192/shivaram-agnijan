def askString():
    x = input()
    a, b = x.split(" ")
    a, b = a.upper(), b.upper()
    return a, b

def resultmsg(sheldon, raj):
    if sheldon == raj:
        return print("De novo!")

    if sheldon == "TESOURA" and (raj == "PAPEL" or raj == "LAGARTO"):
        return print("Bazinga!")

    if sheldon == "PAPEL" and (raj == "PEDRA" or raj == "SPOCK"):
        return print("Bazinga!")

    if sheldon == "PEDRA" and (raj == "LAGARTO" or raj == "TESOURA"):
        return print("Bazinga!")

    if sheldon == "LAGARTO" and (raj == "SPOCK" or raj == "PAPEL"):
        return print("Bazinga!")

    if sheldon == "SPOCK" and (raj == "TESOURA" or raj == "PEDRA"):
        return print("Bazinga!")

    return print("Raj trapaceou!")




sheldon, raj = askString()
resultmsg(sheldon, raj)



